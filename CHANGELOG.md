# Changelog

## [Unreleased]

## 1.1.0
### Changed
- Updated kit to v1.3 

## 1.0.0

* Released
