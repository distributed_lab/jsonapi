# jsonapi

`jsonapi` is helper package for working to encapsulate reusable stuff related to [JSON API specification][jsonapi-spec].

Documentation is also available via [GoDoc][godoc-url]

## Query

Query helps in parsing JSONAPI-compatible request queries. Query provides methods for working with 
JSONAPI-specific params, like "include", "sort", "filter" and "page".

To include in it your handler:

```go
package mypkg

import (
    "net/http"

    "gitlab.com/distributed_lab/ape"
    "gitlab.com/distributed_lab/ape/problems"
    "gitlab.com/distributed_lab/jsonapi/query"
)


type request struct{
    *query.Query

    Filters struct {
        LastName string `schema:"last_name"`
        FirstName string `schema:"first_name,required"`
    }
}

func GetAuthors (r *http.Request, w http.ResponseWriter) {
    var req request
    var err error

    req.Query, err = query.New(r, query.QueryOpts{
        // Will return error query contains anything else that:
        SupportedIncludes:map[string]struct{}{
            "books": {},
            "articles": {},
        },
        SupportedFilters:map[string]struct{}{
            "first_name": {},
            "last_name": {},
        },
        SupportedSorts:map[string]struct{}{
            "age": {},
            "-age": {},
        },
    })

    _ = req.PopulateFilters(&req.Filters)

    if err != nil {
        // all errors are compatible with ape and can be rendered.
        ape.RenderErr(w, problems.BadRequest(err)...)
        return
    }

    if req.Filters.LastName != "" {
        // Filter your data
    }

    if req.ShouldInclude("books") {
        // Make your joins
    }

    for _, sort := range req.SortingParams () {
        // Apply sorging params
    }

    pageParams := req.OffsetPageParams()
    // you can use pageParams.ApplyTo(sql) with your sql statement or
    // directly take:
    //  pageParams.Limit
    //  pageParams.PageNumber
    //  pageParams.Order

    var responseLinks = req.OffsetPageLinks()
    // "self" and "next" links to be pasted directly to the resposne
}
```


[jsonapi-spec]: https://jsonapi.org
[godoc-url]: https://godoc.org/gitlab.com/distributed_lab/jsonapi
