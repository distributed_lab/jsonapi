package query_test

import (
	"errors"
	"testing"

	. "github.com/go-ozzo/ozzo-validation"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/distributed_lab/jsonapi/query"
)

func TestNewFilters(t *testing.T) {
	t.Run("properly populates result filters when they are supported", func(t *testing.T) {
		var supportedFilters = map[string]struct{}{
			"first_name": {},
			"last_name":  {},
		}
		r := GET("/users?filter[first_name]=John&filter[last_name]=Doe")

		res, err := query.NewFilters(r, supportedFilters)

		require.NoError(t, err)

		var filters struct {
			FirstName string `schema:"first_name"`
			LastName  string `schema:"last_name"`
		}

		err = res.PopulateFilters(&filters)

		require.NoError(t, err)

		assert.Equal(t, "John", filters.FirstName)
		assert.Equal(t, "Doe", filters.LastName)
	})

	t.Run("returns proper errors", func(t *testing.T) {
		t.Run("when client provided not-supported filter", func(t *testing.T) {
			var supportedFilters = map[string]struct{}{
				"first_name": {},
				"last_name":  {},
			}

			r := GET("/users?filter[first_name]=John&filter[last_name]=Doe&filter[min_age]=19")

			res, err := query.NewFilters(r, supportedFilters)

			require.Nil(t, res)

			expectedErr := Errors{
				"filter[min_age]": errors.New("`filter[min_age]` is not supported; supported values: `[first_name last_name]`"),
			}

			assertEqualErrs(t, expectedErr, err)
		})

		t.Run("when failed to cast provided value to expected", func(t *testing.T) {
			var supportedFilters = map[string]struct{}{
				"min_age": {},
			}

			r := GET("/users?filter[min_age]=kerwnkrjwq")

			res, err := query.NewFilters(r, supportedFilters)
			require.NoError(t, err)

			var filters struct {
				MinAge int64 `schema:"min_age"`
			}

			err = res.PopulateFilters(&filters)

			expectedErr := Errors{
				"filter[min_age]": errors.New("expected value to be valid int64"),
			}

			assertEqualErrs(t, expectedErr, err)
		})

		t.Run("when didn't provide required filter", func(t *testing.T) {
			var supportedFilters = map[string]struct{}{
				"min_age": {},
			}

			r := GET("/users")
			res, err := query.NewFilters(r, supportedFilters)
			require.NoError(t, err)

			var filters struct {
				MinAge int64 `schema:"min_age,required"`
			}

			err = res.PopulateFilters(&filters)

			expectedErr := Errors{
				"filter[min_age]": errors.New("is required and cannot be blank"),
			}

			assertEqualErrs(t, expectedErr, err)
		})
	})
}
