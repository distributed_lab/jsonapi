package query_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/distributed_lab/jsonapi/query"
)

func TestNewIncludes(t *testing.T) {
	t.Run("properly handles supported includes", func(t *testing.T) {
		var supportedIncludes = map[string]struct{}{
			"author":    {},
			"publisher": {},
			"comments":  {},
		}

		r := GET("/article?include=author,comments")

		res, err := query.NewIncludes(r, supportedIncludes)

		require.NoError(t, err)
		assert.True(t, res.ShouldInclude("author"))
		assert.True(t, res.ShouldInclude("comments"))
		assert.False(t, res.ShouldInclude("publisher"))
	})

	t.Run("properly handles nested includes", func(t *testing.T) {
		t.Run("consider parent resource as needed to include when only child is requested", func(t *testing.T) {
			var supportedIncludes = map[string]struct{}{
				"comments":        {},
				"comments.author": {},
			}

			r := GET("/article?include=comments.author")

			res, err := query.NewIncludes(r, supportedIncludes)

			require.NoError(t, err)
			assert.True(t, res.ShouldInclude("comments"))
		})

		t.Run("panics on ShouldInclude call for parent resource, when only child is supported", func(t *testing.T) {
			var supportedIncludes = map[string]struct{}{
				"comments.author": {},
			}

			r := GET("/article?include=comments.author")

			res, err := query.NewIncludes(r, supportedIncludes)

			require.NoError(t, err)
			assert.Panics(t, func() { _ = res.ShouldInclude("comments") })
		})
	})
}
