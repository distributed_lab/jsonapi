package query

import (
	"fmt"
	"net/http"
	"strings"

	. "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

// Filters is a helper struct to to be used for parsing "filter" query
// params. Filters parses the query to find "filter[...]" params that can later
// be used in your request handler to populate your filter struct.
type Filters struct {
	values          map[string][]string
	supportedValues map[string]struct{}
}

// NewFilters returns new instance of Filters. Accepts "supportedValues" param
// that defines values of "filter[...]" parameter supported for your request. If query contains
// any "filter[...]" param that is not supported, NewFilters will return an
// error (error can be passed directly to ape's problems.BadRequest(err) and rendered to client).
func NewFilters(r *http.Request, supportedValues map[string]struct{}) (*Filters, error) {
	result := Filters{
		values:          make(map[string][]string),
		supportedValues: supportedValues,
	}

	for queryParam, values := range r.URL.Query() {
		if !strings.Contains(queryParam, "filter[") {
			continue
		}

		if len(values) == 0 {
			continue
		}

		filterKey := strings.TrimPrefix(queryParam, "filter[")
		filterKey = strings.TrimSuffix(filterKey, "]")

		if filterKey == "" {
			return nil, Errors{
				queryParam: errors.New("key must not be empty"),
			}
		}

		if len(values) > 1 {
			return nil, Errors{
				queryParam: errors.New("multiple values per one filter are not supported"),
			}
		}

		if _, supported := supportedValues[filterKey]; !supported {
			err := errors.Errorf("`%s` is not supported; supported values: `%v`", queryParam, getSliceOfSupportedValues(supportedValues))
			return nil, Errors{
				queryParam: err,
			}
		}

		result.values[filterKey] = []string{values[0]}
	}

	return &result, nil
}

// PopulateFilters takes a structure with `schema:""` tags and populates
// it with filter query params. Query param should be wrapped in "filter[]" to
// be considered as filter.
// When defining your structure, don't use "filter[key]", just set it's key
// to the schema tag. For example, here:
//
// type Filters struct {
// 		Address string `schema:"address"`
// }
//
// PopulateFilters will take the string value of `filter[address]` query param
// and assign it's value to `Address` field.
func (f *Filters) PopulateFilters(dest interface{}) error {
	switch errs := schema.NewDecoder().Decode(dest, f.values).(type) {
	case nil:
		return nil
	case schema.MultiError:
		res := Errors{}

		for _, err := range errs {
			switch e := err.(type) {
			case schema.ConversionError:
				res[fmt.Sprintf("filter[%s]", e.Key)] = errors.Errorf("expected value to be valid %v", e.Type)
			case schema.EmptyFieldError:
				res[fmt.Sprintf("filter[%s]", e.Key)] = errors.New("is required and cannot be blank")
			case schema.UnknownKeyError:
				res[fmt.Sprintf("filter[%s]", e.Key)] = errors.New("invalid parameter")
			default:
				return Errors{
					"filter": errors.New("failed to decode filter params"),
				}
			}
		}

		return res.Filter()
	default:
		return Errors{
			"filter": errors.New("failed to decode filter params"),
		}
	}
}
