package query

import (
	"net/http"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

// Query is a helper for parsing JSONAPI-compatible request queries. Query
// provides methods for working with JSONAPI-specific params, like "include",
// "sort", "filter" and "page".
type Query struct {
	*Includes
	*Filters
	*Pages
	*Sorts
}

// QueryOpts a set of options to initialize a new Query instance.
type QueryOpts struct {
	SupportedIncludes map[string]struct{}
	SupportedFilters  map[string]struct{}
	SupportedSorts    map[string]struct{}
}

// New returns new instance of Query. If request query is considered
// invalid, a validation error will be returned. Such kind of error
// can be passed to problems.BadRequest(err) for being rendered directly to client.
func New(r *http.Request, opts QueryOpts) (*Query, error) {
	b := Query{}

	var err error

	if b.Includes, err = NewIncludes(r, opts.SupportedIncludes); err != nil {
		return nil, err
	}
	if b.Sorts, err = NewSorts(r, opts.SupportedSorts); err != nil {
		return nil, err
	}
	if b.Filters, err = NewFilters(r, opts.SupportedFilters); err != nil {
		return nil, err
	}
	if b.Pages, err = NewPages(r); err != nil {
		return nil, err
	}

	return &b, nil
}

func getSliceOfSupportedValues(supportedValues map[string]struct{}) []string {
	result := make([]string, 0, len(supportedValues))
	for include := range supportedValues {
		result = append(result, include)
	}

	return result
}

func getSliceOfCommaSeparatedValues(r *http.Request, paramName string) ([]string, error) {
	for queryParam, values := range r.URL.Query() {
		if queryParam != paramName {
			continue
		}

		if len(values) > 1 {
			err := errors.Errorf("multiple '%s' params are not supported, use comma (',') to separate them", paramName)
			return nil, validation.Errors{
				paramName: err,
			}
		}

		if values[0] == "" {
			return nil, nil
		}

		return strings.Split(values[0], ","), nil
	}

	return nil, nil
}
