package query_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/distributed_lab/jsonapi/query"
)

func TestNewSorts(t *testing.T) {
	t.Run("properly maps provided query to SortParams", func(t *testing.T) {
		var supportedSorts = map[string]struct{}{
			"-first_name": {},
			"last_name":   {},
			"-min_age":    {},
		}

		r := GET("/authors?sort=-first_name,last_name,-min_age")

		s, err := query.NewSorts(r, supportedSorts)
		require.NoError(t, err)

		params := s.SortingParams()

		assert.Equal(t, query.SortingParam{Key: "first_name", Order: query.SortOrderDesc}, params[0])
		assert.Equal(t, query.SortingParam{Key: "last_name", Order: query.SortOrderAsc}, params[1])
		assert.Equal(t, query.SortingParam{Key: "min_age", Order: query.SortOrderDesc}, params[2])
	})
}
