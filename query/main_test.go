package query_test

import (
	"net/http"
	"net/url"
	"testing"

	. "github.com/go-ozzo/ozzo-validation"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// GET returns an instance of GET request with provided url.
func GET(rawURL string) *http.Request {
	u, err := url.Parse(rawURL)
	if err != nil {
		panic(err)
	}

	return &http.Request{
		Method: "GET",
		URL:    u,
	}
}

func assertEqualErrs(t *testing.T, expected Errors, actual error) {
	require.Error(t, actual)
	require.IsType(t, Errors{}, actual)

	err := actual.(Errors)

	for field := range expected {
		require.Contains(t, actual, field)
		assert.EqualError(t, err[field], expected[field].Error())
	}

	for field := range err {
		require.Contains(t, expected, field)
		assert.EqualError(t, err[field], expected[field].Error())
	}
}
