package query

import (
	"net/http"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

// SortOrder tells us how we should sort results.
type SortOrder int

const (
	// SortOrderAsc tells us we should sort results in ascending order.
	SortOrderAsc SortOrder = iota
	// SortOrderDesc t ells use we should sort results in descending order.
	SortOrderDesc
)

// Sorts is a helper that is useful to parse "sort" request params. Sorts
// parses the query to extract "sort" params, then can be used to define
// whether the the result query should be sorted by some fields or not.
type Sorts struct {
	params          []SortingParam
	supportedValues map[string]struct{}
}

// NewSorts returns new instance of Sorts. Accepts "supportedValues" param
// that defines values of "sort" parameter supported for your request. It differs
// ascending and descending params, so if you wan't to support both asc/desc sorting for
// your param pass "name" as well as "-name" to the supportedValues.
func NewSorts(r *http.Request, supportedValues map[string]struct{}) (*Sorts, error) {
	values, err := getSliceOfCommaSeparatedValues(r, "sort")
	if err != nil {
		return nil, err
	}

	result := Sorts{
		supportedValues: supportedValues,
		params:          make([]SortingParam, 0, len(values)),
	}

	for _, v := range values {
		var key string
		var order SortOrder

		if _, ok := supportedValues[v]; !ok {
			err = errors.Errorf("`%s` is not supported; supported values: `%v`", v, getSliceOfSupportedValues(supportedValues))
			return nil, validation.Errors{
				"sort": err,
			}
		}

		if strings.HasPrefix(v, "-") {
			key = strings.TrimPrefix(v, "-")
			order = SortOrderDesc
		} else {
			key = v
			order = SortOrderAsc
		}

		result.params = append(result.params, SortingParam{
			Key:   key,
			Order: order,
		})
	}

	return &result, nil
}

// SortingParam defines a singe param for sorting the result. Key is
// the name of key from url param, order is how we should order the results (asc/desc).
type SortingParam struct {
	Key   string
	Order SortOrder
}

// SortingParams returns the params for sorting the result.
func (s *Sorts) SortingParams() []SortingParam {
	return s.params
}
