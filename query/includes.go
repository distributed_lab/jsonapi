package query

import (
	"net/http"
	"strings"

	. "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

// Includes is a helper that is useful to parse "include"
// request params. Includes parses the query to extract "include" params,
// then can be used to define what resources client requested to include.
type Includes struct {
	values          map[string]struct{}
	supportedValues map[string]struct{}
}

// NewIncludes returns new instance of Includes. Accepts "supportedValues" param
// that defines values of "include" parameter supported for your request. If query contains
// any value in the "include" param that is not supported, NewIncludes will return an
// error (error can be passed directly to problems.BadRequest(err) and rendered to client).
func NewIncludes(r *http.Request, supportedValues map[string]struct{}) (*Includes, error) {
	result := Includes{
		values:          make(map[string]struct{}),
		supportedValues: supportedValues,
	}

	values, err := getSliceOfCommaSeparatedValues(r, "include")
	if err != nil {
		return nil, err
	}

	for _, value := range values {
		if _, supported := supportedValues[value]; !supported {
			err = errors.Errorf("`%s` is not supported; supported values`: %v", values, getSliceOfSupportedValues(supportedValues))
			return nil, Errors{
				"include": err,
			}
		}

		// Note: Because compound documents require full linkage (except when relationship linkage is excluded by
		// sparse fieldsets), intermediate resources in a multi-part path must be returned along with the leaf nodes.
		// For example, a response to a request for comments.author should include comments as well as the author of
		// each of those comments.
		subValues := strings.Split(value, ".")
		parentValue := ""
		for i := range subValues {
			if parentValue == "" {
				parentValue = subValues[i]
			} else {
				parentValue += "." + subValues[i]
			}
			result.values[parentValue] = struct{}{}
		}
	}

	return &result, nil
}

// ShouldInclude - returns true if client requested to include resource. ShouldInclude
// will panic if value to check is not listed in supported includes.
func (i *Includes) ShouldInclude(v string) bool {
	if _, ok := i.supportedValues[v]; !ok {
		panic(errors.From(errors.New("unexpected include check of the request"), logan.F{
			"supported_includes": getSliceOfSupportedValues(i.supportedValues),
			"checking_include":   v,
		}))
	}

	_, ok := i.values[v]

	return ok
}
