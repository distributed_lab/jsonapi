package query

import (
	"net/http"
	"strconv"
	"strings"

	. "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

// Pages is a helper struct to that is useful to decode page params if they are present
// in request url and easily populate response links.
type Pages struct {
	r *http.Request

	offsetParams pgdb.OffsetPageParams
	cursorParams pgdb.CursorPageParams
}

const (
	defaultPageCursor = 0
	defaultPageNumber = 0
	defaultPageSize   = 10
	defaultPageOrder  = pgdb.OrderTypeDesc

	maxPageSize = 100

	pageParamNumber = "page[number]"
	pageParamSize   = "page[size]"
	pageParamLimit  = "page[limit]"
	pageParamOrder  = "page[order]"
	pageParamCursor = "page[cursor]"
)

// NewPages returns new instance of Pages. If query doesn't contain any page params -
// pages will use default values instead.
func NewPages(r *http.Request) (*Pages, error) {
	result := Pages{
		r: r,
		cursorParams: pgdb.CursorPageParams{
			Cursor: defaultPageCursor,
			Order:  defaultPageOrder,
			Limit:  defaultPageSize,
		},
		offsetParams: pgdb.OffsetPageParams{
			PageNumber: defaultPageNumber,
			Limit:      defaultPageSize,
			Order:      defaultPageOrder,
		},
	}

	for queryParam, values := range r.URL.Query() {
		if !strings.Contains(queryParam, "page[") {
			continue
		}

		if len(values) == 0 {
			continue
		}

		if len(values) > 1 {
			return nil, Errors{
				queryParam: errors.New("multiple values per one page param are not supported"),
			}
		}

		var err error

		switch queryParam {
		case pageParamNumber:
			result.offsetParams.PageNumber, err = strconv.ParseUint(values[0], 10, 64)
			if err != nil {
				return nil, Errors{
					pageParamNumber: errors.New("should be a valid integer"),
				}
			}
		case pageParamSize, pageParamLimit:
			v, err := strconv.ParseUint(values[0], 10, 64)
			if err != nil {
				return nil, Errors{
					pageParamSize: errors.New("should be a valid integer"),
				}
			}
			result.offsetParams.Limit = v
			result.cursorParams.Limit = v
		case pageParamOrder:
			switch v := values[0]; v {
			case pgdb.OrderTypeAsc, pgdb.OrderTypeDesc:
				result.offsetParams.Order = v
				result.cursorParams.Order = v
			default:
				return nil, Errors{
					pageParamOrder: errors.Errorf("`%s` is not supported, supported values: `%v`", v, []string{pgdb.OrderTypeDesc, pgdb.OrderTypeAsc}),
				}
			}
		case pageParamCursor:
			result.cursorParams.Cursor, err = strconv.ParseUint(values[0], 10, 64)
			if err != nil {
				return nil, Errors{
					pageParamCursor: errors.New("should be a valid integer"),
				}
			}
		default:
			return nil, Errors{
				queryParam: errors.Errorf("`%s` is not supported, supported params: `%v`", queryParam, getSliceOfSupportedValues(map[string]struct{}{
					pageParamNumber: {},
					pageParamSize:   {},
					pageParamOrder:  {},
					pageParamCursor: {},
				})),
			}
		}
	}

	if result.offsetParams.Limit > maxPageSize {
		return nil, Errors{
			pageParamSize: errors.Errorf("exceeds maximum page size of %d", maxPageSize),
		}
	}

	return &result, nil
}

// OffsetPageParams returns decoded offset params.
func (p *Pages) OffsetPageParams() pgdb.OffsetPageParams {
	return p.offsetParams
}

// CursorPageParams returns decoded cusor params.
func (p *Pages) CursorPageParams() pgdb.CursorPageParams {
	return p.cursorParams
}

// Links represent links to be returned in result repsonse.
type Links struct {
	Next string `json:"next,omitempty"`
	Self string `json:"self"`
}

// OffsetPageLinks returns offset-based links that are ready to be returned in response.
func (p *Pages) OffsetPageLinks() *Links {
	params := p.offsetParams
	self := p.getOffsetLink(params.PageNumber, params.Limit, params.Order)
	next := p.getOffsetLink(params.PageNumber+1, params.Limit, params.Order)

	return &Links{
		Self: self,
		Next: next,
	}
}

// CursorPageLinks returns cursor-based links that are ready to be returned in response.
func (p *Pages) CursorPageLinks(last uint64) *Links {
	params := p.cursorParams
	self := p.getCursorLink(params.Cursor, params.Limit, params.Order)
	next := p.getCursorLink(last, params.Limit, params.Order)

	return &Links{
		Self: self,
		Next: next,
	}
}

func (p *Pages) getCursorLink(cursor, limit uint64, order string) string {
	u := p.r.URL
	query := u.Query()

	query.Set(pageParamCursor, strconv.FormatUint(cursor, 10))
	query.Set(pageParamLimit, strconv.FormatUint(limit, 10))
	query.Set(pageParamOrder, string(order))

	u.RawQuery = query.Encode()
	return u.String()
}

func (p *Pages) getOffsetLink(pageNumber, limit uint64, order string) string {
	u := p.r.URL
	query := u.Query()

	query.Set(pageParamNumber, strconv.FormatUint(pageNumber, 10))
	query.Set(pageParamSize, strconv.FormatUint(limit, 10))
	query.Set(pageParamOrder, string(order))

	u.RawQuery = query.Encode()
	return u.String()
}
