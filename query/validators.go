package query

import (
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation"
)

// ValidateResourceType type returns new rule that validates that provided resource
// type is the same as expected.
func ValidateResourceType(resourceType string) *validation.StringRule {
	var message = fmt.Sprintf("invalid resource type, expected '%s'", resourceType)
	return validation.NewStringRule(func(s string) bool { return s == resourceType }, message)
}
