package query_test

import (
	"errors"
	"net/url"
	"testing"

	. "github.com/go-ozzo/ozzo-validation"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/distributed_lab/jsonapi/query"
	"gitlab.com/distributed_lab/kit/pgdb"
)

func TestNewPages(t *testing.T) {
	t.Run("properly maps provided values to pgdb params", func(t *testing.T) {
		r := GET("/books?page[size]=15&page[number]=3&page[order]=desc")

		p, err := query.NewPages(r)
		require.NoError(t, err)

		assert.EqualValues(t, 15, p.OffsetPageParams().Limit)
		assert.EqualValues(t, 3, p.OffsetPageParams().PageNumber)
		assert.Equal(t, pgdb.OrderTypeDesc, p.OffsetPageParams().Order)
	})

	t.Run("returns proper offset links", func(t *testing.T) {
		r := GET("/books?page[size]=15&page[number]=3&page[order]=desc")

		p, err := query.NewPages(r)
		require.NoError(t, err)

		expectedSelf := "/books?page[number]=3&page[order]=desc&page[size]=15"
		expectedNext := "/books?page[number]=4&page[order]=desc&page[size]=15"

		actualSelf, err := url.QueryUnescape(p.OffsetPageLinks().Self)
		require.NoError(t, err)

		actualNext, err := url.QueryUnescape(p.OffsetPageLinks().Next)
		require.NoError(t, err)

		t.Run("contains proper self link", func(t *testing.T) {
			assert.Equal(t, expectedSelf, actualSelf)
		})
		t.Run("contains proper next link", func(t *testing.T) {
			assert.Equal(t, expectedNext, actualNext)
		})
	})

	t.Run("returns proper errors", func(t *testing.T) {
		t.Run("when provided not supported order", func(t *testing.T) {
			r := GET("/books?page[size]=15&page[number]=3&page[order]=oooo")

			p, err := query.NewPages(r)
			require.Nil(t, p)

			expectedErr := Errors{
				"page[order]": errors.New("`oooo` is not supported, supported values: `[desc asc]`"),
			}
			assertEqualErrs(t, expectedErr, err)
		})
		t.Run("when provided page size more that maximum", func(t *testing.T) {
			r := GET("/books?page[size]=150&page[number]=3&page[order]=desc")

			p, err := query.NewPages(r)
			require.Nil(t, p)

			expectedErr := Errors{
				"page[size]": errors.New("exceeds maximum page size of 100"),
			}
			assertEqualErrs(t, expectedErr, err)
		})
	})
}
